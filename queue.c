#include <stdlib.h>
#include <stdbool.h>

#include "queue.h"

queue *
init_queue(void)
{
    queue *q = (queue *) calloc(1, sizeof(queue));
    if (q == NULL) return NULL;

    q->head = NULL;
    q->tail = NULL;

    return q;
}

void
destroy_queue(queue *q)
{
    q->head = NULL;
    q->tail = NULL;
    free(q);
    q = NULL;
}

bool
enqueue(queue *q, int *data)
{
    node *newnode = (node *) calloc(1, sizeof(node));
    if (newnode == NULL) return false;

    newnode->data = data;
    newnode->next = NULL;

    /* check if this is the first node in our queue */
    if (q->tail == NULL) {
        q->head = newnode;
    }
    else {
        /* if not, add that to the end of the queue */
        q->tail->next = newnode;
    }

    /* update the tail */
    q->tail = newnode;
    return true;
}

void *
dequeue(queue *q)
{
    /* check if we have a node in the first place */
    if (q->head == NULL) {
        return NULL;
    }

    node *tmp = q->head;

    /* first save the head's data */
    void *data = tmp->data;

    /* update the new head */
    q->head = tmp->next;

    /* check if we've reached the end of the queue */
    if (q->head == NULL) {
        /* need to update the tail as well */
        q->tail = NULL;
    }

    /* no longer need the old head */
    free(tmp);
    tmp = NULL;

    return data;
}
