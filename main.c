#include <asm-generic/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <limits.h>
#include <err.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <signal.h>
#include <linux/in.h>
#include <netinet/in.h>

#include "server.h"

int
main(int argc, char *argv[])
{
    if (argc != 2) {
        fprintf(stderr, "Usage: %s port\n", argv[0]);
        _exit(EXIT_FAILURE);
    }
    errno = 0;

    /* convert the given port argument to integer */
    int port = (int) strtol(argv[1], (char **) NULL, 10);

    if (errno != 0) {
        err(EXIT_FAILURE, "Couldn't use %s as the port", argv[1]);
    }

    int sockfd;

    /* create a socket */
    if ((sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_IP)) == -1) {
        err(EXIT_FAILURE, NULL);
    }

    /* configure the socket so that the port and address can be reused */
    int option = 1;
    if ( setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR|SO_REUSEPORT, &option, sizeof(option)) == -1 ) {
        err(EXIT_FAILURE, NULL);
    }

    /* bind address information to the socket */
    struct sockaddr_in server_addr = {
        .sin_family = AF_INET,
        .sin_addr.s_addr = INADDR_ANY,
        .sin_port = htons(port)
    };
    memset(&(server_addr.sin_zero), '\0', sizeof(server_addr.sin_zero));

    if (bind(sockfd, (const struct sockaddr *) &server_addr, sizeof(server_addr)) == -1) {
            err(EXIT_FAILURE, NULL);
    }

    printf("[*] Listening on port %d\n", port);

    server_loop(sockfd);

    return EXIT_SUCCESS;
}
