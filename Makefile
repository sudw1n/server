.PHONY: server

CC = clang
CFLAGS = -O3 -Wall
LDFLAGS = -pthread -s
OBJECTS = server.o queue.o main.o

server: $(OBJECTS)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(OBJECTS)

server.o: server.c server.h
	$(CC) $(CFLAGS) -c -o $@ server.c

queue.o: queue.c queue.h
	$(CC) $(CFLAGS) -c -o $@ queue.c

main.o: main.c
	$(CC) $(CFLAGS) -c -o $@ main.c

clean:
	rm -f *.o
	rm -f ./server
