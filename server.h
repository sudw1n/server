#pragma once
#include <stdbool.h>

/* start listening on connections once a server socket is established */
extern void server_loop(int server_sockfd);
