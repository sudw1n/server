#define _GNU_SOURCE
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <pthread.h>
#include <err.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <linux/in.h>
#include <netinet/in.h>

#include "server.h"
#include "queue.h"

#define THREAD_POOL_SIZE 5
#define SERVER_BACKLOG ((THREAD_POOL_SIZE+1))

#define CRLF "\r\n"

static void * handle_connection(void *arg);
static void do_processing(int fd);
static void handle_signal(int signum);

static pthread_mutex_t thread_mutex;
/* used to signal threads that we have a connection to handle */
static pthread_cond_t thread_cond;

void
server_loop(int server_fd)
{
    pthread_mutex_init(&thread_mutex, NULL);
    pthread_cond_init(&thread_cond, NULL);

    if ( (listen(server_fd, SERVER_BACKLOG)) == -1) {
        err(EXIT_FAILURE, NULL);
    }

    /* initialise the thread queue */
    queue *thread_queue = init_queue();

    /* initialise the thread pool */
    pthread_t thread_pool[THREAD_POOL_SIZE];

    for (int i = 0; i < THREAD_POOL_SIZE; ++i) {
        pthread_create(&thread_pool[i], NULL, handle_connection, thread_queue);
    }

    /* register a signal handler for exiting gracefully, in the event that I hit Ctrl+C, etc. */
    signal(SIGINT, handle_signal);
    signal(SIGTERM, handle_signal);
    signal(SIGQUIT, handle_signal);

    while (true)
    {
        /* start accepting connections */
        struct sockaddr_in client_addr;
        socklen_t addrlen = sizeof(client_addr);
        int *newsockfd = calloc(1, sizeof(int));

        *newsockfd = accept(server_fd, (struct sockaddr *)&client_addr, &addrlen);

        if (*newsockfd == -1)
        {
            warn("accept");
            continue;
        }

        /* retrieve and display the client's information */
        char *client_ip = calloc(INET_ADDRSTRLEN, sizeof(char));
        int *client_port = calloc(1, sizeof(int));

        *client_port = ntohs(client_addr.sin_port);
        if ( inet_ntop(AF_INET, &(client_addr.sin_addr), client_ip, INET_ADDRSTRLEN) == NULL ) {
            warn("Couldn't determine client's IP");
        }
        else {
            printf("[*] Got connection from %s:%d\n", client_ip, *client_port);
        }

        free(client_ip);
        client_ip = NULL;
        free(client_port);
        client_port = NULL;

        /* add the new socket into the queue */
        pthread_mutex_lock(&thread_mutex);

        enqueue(thread_queue, newsockfd);

        /* signal to the threads that we have work to do */
        pthread_cond_signal(&thread_cond);
        pthread_mutex_unlock(&thread_mutex);
    }

    pthread_cond_destroy(&thread_cond);
    pthread_mutex_destroy(&thread_mutex);
}

void *
handle_connection(void *thread_queue)
{
    int *client_socket = NULL;
    while (true)
    {
        pthread_mutex_lock(&thread_mutex);
        /* wait until we get a signal */
        pthread_cond_wait(&thread_cond, &thread_mutex);

        /* retrieve the socket from the queue */
        client_socket = dequeue(thread_queue);

        pthread_mutex_unlock(&thread_mutex);

        /* handle the connection is there is one */
        if (client_socket != NULL) {
            printf("[*] Thread handling connection on FD: %d\n", *client_socket);
            do_processing(*client_socket);
            printf("[*] Thread closing connection on FD: %d\n", *client_socket);

            /* clean up */
            close(*client_socket);
            free(client_socket);
            client_socket = NULL;
        }
    }

    pthread_exit(NULL);
}

void
do_processing(int client_socket)
{
    FILE *in = fdopen(client_socket, "r");
    FILE *out = fdopen(client_socket, "w");

    /* disable buffering on the streams */
    setvbuf(in, NULL, _IONBF, 0);
    setvbuf(out, NULL, _IONBF, 0);

    char *buf;
    fprintf(out, "file: ");
    if ( fscanf(in, "%ms", &buf) < 0 ) {
        fprintf(stderr, "[!] Failed to read request from client. Exiting..." CRLF CRLF);
        return;
    }

    fputs("\nSorry this server is a work in progress." CRLF, out);
    fputs("For now, I'm just going to send you back your request." CRLF, out);

    fprintf(out, "Request: %s" CRLF CRLF, buf);
    free(buf);
    buf = NULL;
}

void
handle_signal(int signum)
{
    fprintf(stderr, "[!] Received %s signal. Server shutting down!\n", sigabbrev_np(signum));
    _exit(EXIT_FAILURE);
}
