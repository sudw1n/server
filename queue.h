#pragma once
#include <stdbool.h>

typedef struct node
{
    void *data;
    struct node *next;
} node;

typedef struct queue {
    node *head;
    node *tail;
} queue;

/* initialise a new queue */
extern queue *init_queue(void);
/* deallocate the given queue */
extern void destroy_queue(queue *q);
/* create and add a node to the queue initialised with the given data.
 * Returns false if memory couldn't be allocated for the new node.
 */
extern bool enqueue(queue *q, int *data);
/* returns the data contained in the head of the queue */
extern void *dequeue(queue *q);
