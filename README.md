Built as a just-for-fun project, this is a work-in-progress web server capable of handling multiple
requests. Each individual request is handled by a separate thread from the thread pool. Currently,
this server doesn't render any functionality and instead just displays a message to the client.

## How to Compile?
Clone the project into a local folder, and run `make -j3`. You need to have `clang` installed, if
not change the value of `CC` in `Makefile`.

## Usage
Run `./server <port>`. If the listening port is below 1024, then the binary must be run with
root privileges.
Hit `CTRL+C` to stop the server.
